package main

import (
  "flag"
  "fmt"
  "encoding/csv"
  "os"
  "io"
  "strings"
)

func main() {
  // create a pointer with default values for csv parameter
  // comment will be displayed with "-h"
  csvPtr := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
  flag.Parse()
  //fmt.Println("csv:", *csvPtr)

  // Open csv file
  csvFile, error := os.Open(*csvPtr)
  if error != nil {
    fmt.Println("Error:", error)
  }
  defer csvFile.Close()

  // initialize variables
  nbQuestions := 0
  nbGoodAnswers := 0

  // read line by line until EOF
  reader := csv.NewReader(csvFile)
  for {
      line, error := reader.Read()
      // io.EOF is the end of the file
      if error == io.EOF {
          break
      } else if error != nil {
        fmt.Println("Error:", error)
      }

      // count the number of questions
      nbQuestions = nbQuestions + 1

      // Print question to screen
      fmt.Println("Problem #", nbQuestions, ":", line[0], "=")

      // Scan answer from user input
      var answer string
      fmt.Scanln(&answer)
      //fmt.Println("Your answer:", answer)

      // compare user input to the answer from file
      result := strings.Compare(line[1], answer)
      /*
      if (result == 0) {
        fmt.Println("Good answer")
      }else {
        fmt.Println("Bad answer")
      }
      */

      // Count good answers
      if (result == 0) {
        nbGoodAnswers = nbGoodAnswers + 1
      }
  }
  fmt.Println("You scored", nbGoodAnswers, "out of", nbQuestions)
}
